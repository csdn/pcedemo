package com.csdn.pce.api;

import java.util.List;

public interface PceService<V,E> {

    int getLinkCount();

    int getNodeCount();

    List<E> getPath(V source, V target);
}
