package com.csdn.pce.api;

public class Link {

    public Link(int localId, int localPort, int remoteId, int remotePort) {
        this.localSystemId = localId;
        this.localPort = localPort;
        this.remoteSystemId = remoteId;
        this.remotePort = remotePort;
    }

    public int getLocalSystemId() {
        return localSystemId;
    }

    public void setLocalSystemId(int localSystemId) {
        this.localSystemId = localSystemId;
    }

    public int getRemoteSystemId() {
        return remoteSystemId;
    }

    public void setRemoteSystemId(int remoteSystemId) {
        this.remoteSystemId = remoteSystemId;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    public int getLocalPort() {
        return localPort;
    }

    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }

    @Override
    public String toString() {
        return remoteSystemId + ":" + remotePort + "->" + localSystemId + ":" + localPort;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof Link)) {
            return false;
        }
        if (this == other) {
            return true;
        }
        Link otherLink = (Link)other;
        return this.localSystemId == otherLink.getLocalSystemId()
                && this.localPort == otherLink.getLocalPort()
                && this.remoteSystemId == otherLink.getRemoteSystemId()
                && this.remotePort == otherLink.getRemotePort();
    }

    @Override
    public int hashCode() {
        return localSystemId + localPort + remoteSystemId + remotePort;
    }

    private int localSystemId;
    private int localPort;
    private int remoteSystemId;
    private int remotePort;
}
