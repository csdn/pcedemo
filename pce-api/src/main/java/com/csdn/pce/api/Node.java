package com.csdn.pce.api;

public class Node {

    public Node(int id) {
        nodeId = id;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int id) {
        this.nodeId = id;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof Node)) {
            return false;
        }
        if (this == other) {
            return true;
        }

        return nodeId == ((Node)other).getNodeId();
    }

    @Override
    public int hashCode() {
        return this.nodeId;
    }

    private int nodeId;
}
