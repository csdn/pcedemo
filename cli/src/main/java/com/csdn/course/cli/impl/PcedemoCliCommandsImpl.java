/*
 * Copyright © 2018 csdn and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.csdn.course.cli.impl;

import com.csdn.course.cli.api.PcedemoCliCommands;
import com.csdn.pce.api.Link;
import com.csdn.pce.api.Node;
import com.csdn.pce.api.PceService;

import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PcedemoCliCommandsImpl implements PcedemoCliCommands {

    private static final Logger LOG = LoggerFactory.getLogger(PcedemoCliCommandsImpl.class);
    private PceService pceService;

    public PcedemoCliCommandsImpl(PceService pceService) {
        LOG.info("PcedemoCliCommandImpl initialized");
        this.pceService = pceService;
    }

    @Override
    public String queryCommand(String queryArgument) {
        int nodecount = pceService.getNodeCount();
        int linkcount = pceService.getLinkCount();
        return "network topology: node count=" + nodecount + ";link count=" + linkcount;
    }

    @Override
    public  String shortestPathCommand(int source, int destination) {
        List<Link> path =  pceService.getPath(new Node(source),new Node(destination));
        StringBuffer strBuffer = new StringBuffer();
        Iterator<Link> iterator = path.iterator();
        while (iterator.hasNext()) {
            strBuffer.append(iterator.next().toString());
            strBuffer.append("->");
        }
        strBuffer.delete(strBuffer.lastIndexOf("->"), strBuffer.length());
        return strBuffer.toString();
    }
}
