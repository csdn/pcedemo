/*
 * Copyright (c) 2015 Cisco Systems, Inc. and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.csdn.pceparent.karafutil;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;

import org.apache.karaf.deployer.blueprint.BlueprintURLHandler;
import org.apache.karaf.deployer.features.FeatureURLHandler;

public class CustomBundleUrlStreamHandlerFactory implements URLStreamHandlerFactory {

    private static final String MVN_URI_PREFIX = "mvn";
    private static final String WRAP_URI_PREFIX = "wrap";
    private static final String FEATURE_URI_PREFIX = "feature";
    private static final String BLUEPRINT_URI_PREFIX = "blueprint";

    @Override
    public URLStreamHandler createURLStreamHandler(String protocol) {
        switch (protocol) {
            case MVN_URI_PREFIX:
                return new org.ops4j.pax.url.mvn.Handler();
            case WRAP_URI_PREFIX:
                return new org.ops4j.pax.url.wrap.Handler();
            case FEATURE_URI_PREFIX:
                return new FeatureURLHandler();
            case BLUEPRINT_URI_PREFIX:
                return new BlueprintURLHandler();
            default:
                return null;
        }
    }

}
