package com.csdn.pce.impl;

import com.csdn.pce.api.Link;
import com.csdn.pce.api.Node;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.lmax.disruptor.EventFactory;

final class LinkEvent {
    static final EventFactory<LinkEvent> FACTORY = LinkEvent::new;
    private SettableFuture<Void> future;
    private Link link;
    private Node node1;
    private Node node2;
    private PceServiceImpl topo;

    private LinkEvent() {

    }

    @SuppressWarnings("checkstyle:hiddenField")
    ListenableFuture<Void> initialize(final Link link,Node node1,Node node2,final PceServiceImpl topo) {
        this.link = link;
        this.topo = topo;
        this.node1 = node1;
        this.node2 = node2;
        return SettableFuture.create();
    }

    void updateLink() {
        topo.addLink(link,node1,node2);
    }

    void setFuture() {
        if (future != null) {
            future.set(null);
        }
        else {
            SettableFuture.create().set(null);
        }
    }
}
