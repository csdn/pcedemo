/*
 * Copyright © 2018 csdn and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.csdn.pce.impl;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PcedemoProvider {

    private static final Logger LOG = LoggerFactory.getLogger(PcedemoProvider.class);

    ServerBootstrap serverBootstrap = new ServerBootstrap();
    EventLoopGroup bossGroup = new NioEventLoopGroup(1);
    EventLoopGroup workerGroup = new NioEventLoopGroup();
    ChannelFuture channelFuture;
    Channel channel;
    private PceServiceImpl networktopo;

    public PcedemoProvider(PceServiceImpl networktopo) {
        this.networktopo = networktopo;
    }

    /**
     * Method called when the blueprint container is created.
     */
    public void init() {
        LOG.info("PcedemoProvider Session Initiated");

        serverBootstrap.group(bossGroup,workerGroup)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG,1024)
                .childOption(ChannelOption.TCP_NODELAY,true)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new ServerInitializer(networktopo));
        channelFuture = serverBootstrap.bind(6666);

        try {
            this.channel = channelFuture.sync().channel();
        } catch (InterruptedException e) {
            LOG.warn("channel close ex:",e);
        }
    }

    /**
     * Method called when the blueprint container is destroyed.
     */
    public void close() {
        LOG.info("PcedemoProvider Closed");
        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
    }
}