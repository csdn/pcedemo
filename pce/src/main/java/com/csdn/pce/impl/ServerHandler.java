package com.csdn.pce.impl;

import com.csdn.pce.api.Link;
import com.csdn.pce.api.Node;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.InsufficientCapacityException;
import com.lmax.disruptor.PhasedBackoffWaitStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.net.InetAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Sharable
public class ServerHandler extends SimpleChannelInboundHandler<String> {

    private static final WaitStrategy DEFAULT_STRATEGY = PhasedBackoffWaitStrategy.withLock(
            1L, 30L, TimeUnit.MILLISECONDS);
    private static final Logger LOG = LoggerFactory.getLogger(ServerHandler.class);
    private final Disruptor<LinkEvent> disruptor;
    private static final EventHandler<LinkEvent> DISPATCH_EVENT = (event, sequence, endOfBatch) -> event.updateLink();
    private static final EventHandler<LinkEvent> NOTIFY_FUTURE = (event, sequence, endOfBatch) -> event.setFuture();
    private PceServiceImpl topo;
    private static int conn_count = 1;
    private boolean enableDisruptor = false;

    public ServerHandler() {
        final ExecutorService executor = Executors.newCachedThreadPool();
        disruptor = new Disruptor<>(LinkEvent.FACTORY,
                65536 * 16, executor, ProducerType.MULTI, DEFAULT_STRATEGY);
        disruptor.handleEventsWith(DISPATCH_EVENT);
        //disruptor.after(DISPATCH_EVENT).handleEventsWith(NOTIFY_FUTURE);
        disruptor.start();
    }

    public void setTopo(PceServiceImpl topo) {
        this.topo = topo;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // 为新连接发送庆祝
        ctx.write("Welcome to " + InetAddress.getLocalHost().getHostName() + "!\r\n");
        ctx.write("You are the No. " + (conn_count ++) + " user.\r\n");
        ctx.flush();
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, String input) {

        //LOG.info(input);
        String[] tmp = input.split(";");
        if (tmp[0].equalsIgnoreCase("link")) {
            Link link = new Link(Integer.parseInt(tmp[1]), Integer.parseInt(tmp[2]), Integer.parseInt(tmp[3]),
                    Integer.parseInt(tmp[4]));

            try {
                if (enableDisruptor) {
                    final long seq;

                    seq = disruptor.getRingBuffer().tryNext();
                    final LinkEvent event = disruptor.get(seq);
                    event.initialize(link, new Node(Integer.parseInt(tmp[1])), new Node(Integer.parseInt(tmp[3])), topo);
                    disruptor.getRingBuffer().publish(seq);
                } else {
                    topo.addLinkSync(link, new Node(Integer.parseInt(tmp[1])), new Node(Integer.parseInt(tmp[3])));
                }
            } catch (InsufficientCapacityException | NullPointerException e) {
                LOG.warn(e.getMessage());
            }
        } else if (tmp[0].equalsIgnoreCase("source")) {
            topo.getIncomingEdgeMap(new Node(Integer.parseInt(tmp[1])));
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        LOG.warn(cause.getMessage());
        ctx.close();
    }
}
