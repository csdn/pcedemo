package com.csdn.pce.impl;

import com.csdn.pce.api.Link;
import com.csdn.pce.api.Node;
import com.csdn.pce.api.PceService;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;

import java.util.List;
import java.util.Map;

public class PceServiceImpl implements PceService<Node,Link> {
    private DirectedGraph<Node,Link> graph;
    private DijkstraShortestPath<Node,Link> shortestPath;

    public PceServiceImpl() {
        graph = new DirectedSparseMultigraph();
        shortestPath = new DijkstraShortestPath(graph);
    }

    public void reset() {
        graph = new DirectedSparseMultigraph();
        shortestPath = new DijkstraShortestPath(graph);
    }

    public void addLink(Link link,Node node1,Node node2) {
        graph.addEdge(link, node1, node2);
    }

    public void addLinkSync(Link link,Node node1,Node node2) {
        synchronized (this) {
            graph.addEdge(link, node1, node2);
        }
    }

    @Override
    public List<Link> getPath(Node node1, Node node2) {
        return shortestPath.getPath(node1,node2);
    }

    @Override
    public int getNodeCount() {
        return graph.getVertexCount();
    }

    @Override
    public int getLinkCount() {
        return graph.getEdgeCount();
    }

    public Map<Node,Link> getIncomingEdgeMap(Node source) {
        synchronized (this) {
            return shortestPath.getIncomingEdgeMap(source);
        }
    }
}
