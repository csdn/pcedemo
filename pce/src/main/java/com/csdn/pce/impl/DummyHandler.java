package com.csdn.pce.impl;

import com.csdn.pce.api.Link;
import com.csdn.pce.api.Node;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.InsufficientCapacityException;
import com.lmax.disruptor.PhasedBackoffWaitStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DummyHandler {
    public static volatile int dispatch_count = 1;
    private static final WaitStrategy DEFAULT_STRATEGY = PhasedBackoffWaitStrategy.withLock(
            1L, 30L, TimeUnit.MILLISECONDS);

    private static final Logger LOG = LoggerFactory.getLogger(ServerHandler.class);
    private PceServiceImpl topo;
    private Disruptor<LinkEvent> disruptor;
    private static final EventHandler<LinkEvent> DISPATCH_EVENT = (event, sequence, endOfBatch) -> {
        event.updateLink();
        dispatch_count++;
    };

    private static final EventHandler<LinkEvent> NOTIFY_FUTURE = (event, sequence, endOfBatch) -> event.setFuture();

    public DummyHandler() {
        topo = new PceServiceImpl();
    }

    public void init() {
        final ExecutorService executor = Executors.newCachedThreadPool();
        disruptor = new Disruptor<>(LinkEvent.FACTORY,
                65536 * 64, executor, ProducerType.SINGLE, DEFAULT_STRATEGY);
        disruptor.handleEventsWith(DISPATCH_EVENT);
        //disruptor.after(DISPATCH_EVENT).handleEventsWith(NOTIFY_FUTURE);
        disruptor.start();
    }

    public void setTopo(PceServiceImpl topo) {
        this.topo = topo;
    }

    public void updateLink() {
        Link link = null;
        for (int i = 1; i < 100001; i++) {
            link = new Link(i, 1, i + 1, 2);
            topo.addLink(link,new Node(i), new Node(i + 1));
        }
    }

    public void updateLinkSync() {
        Link link = null;
        for (int i = 1; i < 100001; i++) {
            link = new Link(i, 1, i + 1, 2);
            topo.addLinkSync(link,new Node(i), new Node(i + 1));
        }
    }

    public void updateLinkByDisruptor() {
        long seq = 0;

        Link link = null;
        LinkEvent event = null;
        for (int i = 1; i < 100001; i++) {
            try {
                seq = disruptor.getRingBuffer().tryNext();
            } catch (final InsufficientCapacityException e) {
                //
            }
            link = new Link(i, 1, i + 1, 2);
            event = disruptor.get(seq);
            event.initialize(link, new Node(i), new Node(i + 1), topo);
            disruptor.getRingBuffer().publish(seq);

        }
    }

    public void close() {
        disruptor.shutdown();
    }

    public static void main(String [] args) {

        DummyHandler dummy = new DummyHandler();
        /*for (int i = 0; i < 5; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    long begin = System.currentTimeMillis();
                    dummy.updateLinkSync();
                    long end = System.currentTimeMillis();

                }
            }).start();
        }*/

        /*PceServiceImpl topo = new PceServiceImpl();
        dummy.setTopo(topo);
        for (int i = 0; i < 5; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    long begin = System.currentTimeMillis();
                    dummy.updateLink();
                    long end = System.currentTimeMillis();

                }
            }).start();
        }*/




        dummy.init();
        long begin = System.currentTimeMillis();
        dummy.updateLinkByDisruptor();
        dummy.updateLinkByDisruptor();
        dummy.updateLinkByDisruptor();
        dummy.updateLinkByDisruptor();
        dummy.updateLinkByDisruptor();

        while (dummy.dispatch_count < 499999) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
              //
            }
        }
        long end = System.currentTimeMillis();



        dummy.close();
    }
}
